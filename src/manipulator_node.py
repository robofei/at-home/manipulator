# !/usr/bin/env python

import rospy
from std_msgs.msg import String, Float64
from dynamixel_msgs.msg import JointState
import ArmMod
import numpy as np
import pyglet
import math
import time
from dynamixel_controllers.srv import SetSpeed

rospy.init_node('ArmListener', anonymous=True)

# Publishers
j = rospy.Publisher('/oi', String, queue_size=20)  # Junta 1
joint1 = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=20)  # Junta 1
joint2 = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=20)  # Junta 2
joint3 = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=20)  # Junta 3
joint4 = rospy.Publisher('/tilt5_controller/command', Float64, queue_size=20)  # Pulso
base = rospy.Publisher('/tilt_controller/command', Float64, queue_size=20)  # base



# q_values = [
#     np.round(current_pose_joint1 + (1.98), 2),
#     np.round(current_pose_joint2 - (0.41), 2),
#     np.round(current_pose_joint3 - (0.46), 2), math.pi / 2]
# q_values = [
#     np.round(current_pose_joint1 + (1.98), 2),
#     np.round(current_pose_joint2 - (0.41), 2),
#     np.round(current_pose_joint3 - (0.46), 2), math.pi / 2]
angle = np.array([math.pi / 2, -math.pi / 2, 0, math.pi / 2])

coordX = 220
coordY = 380
coordZ = 140


# Create a Arm3Link
arm = ArmMod.Arm3Link(q=angle, q0=angle, L=np.array([130, 133, 225]))
arm.q = arm.inv_kin(xyz=[coordX, coordY, coordZ])


if not (math.isnan(arm.q[0])):
    # transformations
    q1 = np.round(arm.q[0] - (2.01), 2)
    q2 = np.round(arm.q[1] + (0.32), 2)
    q3 = np.round(arm.q[2] + (0.48), 2)
    q4 = np.round(arm.q[3] + (-1.57), 2)
print(q1,q2,q3,q4)
# (0.63, -1.52, -0.13, 1.57)
# Publishing joint values

rate = rospy.Rate(10)

while not rospy.is_shutdown():
    joint1.publish(q1)
    joint2.publish(q2)
    joint3.publish(q3)
    base.publish(q4)
    j.publish('OI')
    rate.sleep()
