#! /usr/bin/env python

import rospy
import time
import actionlib
from dynamixel_msgs.msg import JointState
import ArmMod
import numpy as np
import pyglet
import math
import time
from dynamixel_controllers.srv import SetSpeed
from manipulator.msg import pointmsgFeedback, pointmsgResult, pointmsgAction, pointmsgGoal
from geometry_msgs.msg import Twist 
from std_msgs.msg import Empty ,String, Float64

PENDING = 0
ACTIVE = 1
DONE = 2
WARN = 3
ERROR = 4


# definition of the feedback callback. This will be called when feedback
# is received from the action server
# it just prints a message indicating a new message has been received
def feedback_callback(feedback):

    print('Prontcho')

# initializes the action client node
rospy.init_node('manip_action_client')

action_server_name = '/manip_action'
client = actionlib.SimpleActionClient(action_server_name, ArdroneAction)

# espera pelo server
rospy.loginfo('Waiting for action Server '+action_server_name)
client.wait_for_server()
rospy.loginfo('Action Server Found...'+action_server_name)

# cria um goal para mandar ao server
goal = ArdroneGoal()
goal.x = 220
goal.y = 380
goal z = 0

client.send_goal(goal, feedback_cb=feedback_callback)



state_result = client.get_state()

rate = rospy.Rate(1)

rospy.loginfo("state_result: "+str(state_result))
    
rospy.loginfo("[Result] State: "+str(state_result))
if state_result == ERROR:
    rospy.logerr("Something went wrong in the Server Side")
if state_result == WARN:
    rospy.logwarn("There is a warning in the Server Side")


