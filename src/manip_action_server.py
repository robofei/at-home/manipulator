#! /usr/bin/env python
import actionlib
import ArmMod
import pyglet
import numpy
import rospy
import time
import math
import time

from manipulator.msg import pointmsgFeedback, pointmsgResult, pointmsgAction, pointmsgGoal
from manipulator.msg import emptyFeedback, emptyResult, emptyAction, emptyGoal
from std_msgs.msg import Empty ,String, Float64
from dynamixel_controllers.srv import SetSpeed
from dynamixel_msgs.msg import JointState
from geometry_msgs.msg import Twist

class pointmsgClass(object):
    # cria msg usadas como feedback
  _feedback = pointmsgFeedback()
  _result   = pointmsgResult()

  def __init__(self):
    self.rate = rospy.Rate(1)

    #cria o action_server
    self._as = actionlib.SimpleActionServer("manipulator_goto", pointmsgAction, self.goal_callback, False)
    self._grip_open = actionlib.SimpleActionServer("manipulator_open", emptyAction, self.open_callback, False)
    self._grip_close = actionlib.SimpleActionServer("manipulator_close", emptyAction, self.close_callback, False)
    self._as.start()
    self._grip_open.start()
    self._grip_close.start()

    # define publishers usados
    self.base = rospy.Publisher('/tilt_controller/command', Float64, queue_size=20)
    self.joint1 = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=20)
    self.joint2 = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=20)
    self.joint3 = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=20)
    self.joint4 = rospy.Publisher('/tilt5_controller/command', Float64, queue_size=20)
    self.grip = rospy.Publisher('/tilt6_controller/command', Float64, queue_size=20)


  def goal_callback(self, goal):
    #callback chamada sempre que o server for chamado por um node

    success = True

    for i in range(0, 4):
      # verificar se nao foi cancelado pelo client
      if self._as.is_preempt_requested():
        rospy.loginfo('The goal has been cancelled/preempted')
        # goal cancelado
        self._as.set_preempted()
        success = False
        break

      angle = numpy.array([math.pi / 2, -math.pi / 2, 0, math.pi / 2])

      # Cria  Arm3Link
      arm = ArmMod.Arm3Link(q=angle, q0=angle, L=numpy.array([130, 133, 225]))
      arm.q = arm.inv_kin(xyz=[goal.x, goal.y, goal.z])

      if not (math.isnan(arm.q[0])):
        # transformadas
        q1 = numpy.round(arm.q[0] - (2.01), 2)
        q2 = numpy.round(arm.q[1] + (0.32), 2)
        q3 = numpy.round(arm.q[2] + (0.48), 2)
        q4 = numpy.round(arm.q[3] + (-1.57), 2)
        # print(q1,q2,q3,q4)
        # Publica juntas
        self.joint1.publish(q1)
        self.joint2.publish(q2)
        self.joint3.publish(q3)
        self.base.publish(q4)
      # computado a 1hz
      self.rate.sleep()

    if success:
      rospy.loginfo("success: goto")
      self._result = Empty()
      self._as.set_succeeded(self._result)

  def open_callback(self, goal):
    #callback chamada sempre que o server for chamado por um node
    self.grip.publish(0.7)
    self.rate.sleep()
    rospy.loginfo("success: open")
    self._grip_open.set_succeeded(Empty())


  def close_callback(self, goal):
    #callback chamada sempre que o server for chamado por um node
    self.grip.publish(-0.3)
    self.rate.sleep()
    rospy.loginfo("success: close")
    self._grip_close.set_succeeded(Empty())


if __name__ == '__main__':
  rospy.init_node('manipulator')
  pointmsgClass()
  rospy.spin()
